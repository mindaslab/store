require "minitest/autorun"
require_relative "./store.rb"

class TestStore < Minitest::Test
  def test_able_to_create_store
    assert_equal Store.new.class, Store
  end

  def test_should_be_able_to_set_key_value_pair
    store = Store.new
    store.set :key, "value"
    assert_equal store.hash[:key], "value"
  end

  def test_should_be_able_to_get_value_for_key
    store = Store.new
    store.set :key, "value"
    assert_equal store.get(:key), "value"
  end

  def test_delete
    store = Store.new
    store.set :key, "value"
    store.delete :key
    assert_equal store.hash.keys.include?(:key), false
  end

  def test_save_and_load
    store = Store.new
    store.set :key, "value"
    store.save "store.pstore"
    store_reader = Store.new
    store_reader.load "store.pstore"
    assert_equal store_reader.get(:key), "value"
  end

  def test_file_name
    store = Store.new
    store.file_name = "store.pstore"
    store.set :key, "value"
    store.save
    store_reader = Store.new
    store_reader.file_name = "store.pstore"
    store_reader.load
    assert_equal store_reader.get(:key), "value"
  end

  def test_initialize_with_file_name
    store = Store.new "store.pstore"
    store.set :key, "value"
    store.save
    store_reader = Store.new "store.pstore"
    store_reader.load
    assert_equal store_reader.get(:key), "value"
  end
end
