require "pstore"

class Store
  attr_reader :hash
  attr_accessor :file_name

  def initialize file_name = nil
    @hash = {}
    @file_name = file_name if file_name
  end

  def set key, value
    @hash[key] = value
  end

  def get key
    @hash[key]
  end

  def delete key
    @hash.delete key
  end

  def save file_name = @file_name
    store = PStore.new(file_name, thread_safe = true)
    store.transaction do
      store[:hash] = @hash
      store.commit
    end
  end

  def load file_name = @file_name
    store = PStore.new(file_name, thread_safe = true)
    store.transaction do
      @hash = store[:hash]
    end
  end
end
